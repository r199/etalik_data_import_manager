<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230807204422 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE clients (id INT AUTO_INCREMENT NOT NULL, business_account VARCHAR(255) DEFAULT NULL, event_account VARCHAR(255) DEFAULT NULL, last_event_account VARCHAR(255) DEFAULT NULL, sheet_number VARCHAR(255) DEFAULT NULL, civility_wording VARCHAR(255) DEFAULT NULL, current_vehicle_owner VARCHAR(255) DEFAULT NULL, name VARCHAR(255) DEFAULT NULL, first_name VARCHAR(255) DEFAULT NULL, number_and_name_of_the_road VARCHAR(255) DEFAULT NULL, complement_address1 VARCHAR(255) DEFAULT NULL, postal_code VARCHAR(255) DEFAULT NULL, city VARCHAR(255) DEFAULT NULL, home_phone VARCHAR(255) DEFAULT NULL, cell_phone VARCHAR(255) DEFAULT NULL, work_phone VARCHAR(255) DEFAULT NULL, email VARCHAR(255) DEFAULT NULL, circulation_date VARCHAR(255) DEFAULT NULL, purchase_date VARCHAR(255) DEFAULT NULL, last_event_date VARCHAR(255) DEFAULT NULL, brand_label VARCHAR(255) DEFAULT NULL, model_label VARCHAR(255) DEFAULT NULL, version VARCHAR(255) DEFAULT NULL, vin VARCHAR(255) DEFAULT NULL, registration VARCHAR(255) DEFAULT NULL, type_of_lead VARCHAR(255) DEFAULT NULL, mileage VARCHAR(255) DEFAULT NULL, energy_label VARCHAR(255) DEFAULT NULL, nvseller VARCHAR(255) DEFAULT NULL, voseller VARCHAR(255) DEFAULT NULL, billing_comment VARCHAR(255) DEFAULT NULL, vnvotype VARCHAR(255) DEFAULT NULL, vnvofile_number VARCHAR(255) DEFAULT NULL, nvsales_intermediary VARCHAR(255) DEFAULT NULL, event_date VARCHAR(255) DEFAULT NULL, event_origin VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE clients');
    }
}
