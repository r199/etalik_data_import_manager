$(function () {
  $("#example1")
    .DataTable({
      responsive: true,
      lengthChange: false,
      autoWidth: false,
      buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
      language: {
        decimal: "",
        emptyTable: "aucune donnée disponible",
        info: "Affichage de _START_ to _END_ entrées sur _TOTAL_",
        infoEmpty: "Affichage de 0 à 0 sur 0 entrées",
        infoFiltered: "(filtré à partir de _MAX_ entrées au total)",
        infoPostFix: "",
        thousands: ",",
        lengthMenu: "Afficher les entrées du _MENU_",
        loadingRecords: "Chargement...",
        processing: "",
        search: "Chercher:",
        zeroRecords: "Aucun enregistrements correspondants trouvés",
        paginate: {
          first: "Première",
          last: "Dernière",
          next: "Prochaine",
          previous: "Précédente",
        },
        aria: {
          sortAscending:
            ": activer pour trier les colonnes par ordre croissant",
          sortDescending:
            ": activer pour trier les colonnes par ordre décroissant",
        },
      },
    })
    .buttons()
    .container()
    .appendTo("#example1_wrapper .col-md-6:eq(0)");
  $("#example2").DataTable({
    paging: true,
    lengthChange: false,
    searching: false,
    ordering: true,
    info: true,
    autoWidth: false,
    responsive: true,
  });
});
