<?php

namespace App\Controller;

use App\Form\ImportType;
use App\Service\ClientService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ImportController extends AbstractController
{
    /**
     * @Route("/", name="app_import")
     */
    public function index(Request $request, ClientService $clientService): Response
    {
        $form = $this->createForm(ImportType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $file = $form->get('file')->getData();
            try {
                $response = $clientService->import($file);
                $this->addFlash('success', $response);
                return $this->redirectToRoute('app_saved_data');
            } catch (\Throwable $th) {
                $this->addFlash('danger','Erreur lors de l\'ajout dans la base de donnée');
            }
        }
        return $this->render('import/index.html.twig',[
            'form' => $form->createView(),
        ]);
    }
}
