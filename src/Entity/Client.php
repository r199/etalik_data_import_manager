<?php

namespace App\Entity;

use App\Repository\ClientRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ClientRepository::class)
 * @ORM\Table(name="clients")
 */
class Client
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $businessAccount;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $eventAccount;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $lastEventAccount;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sheetNumber;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $civilityWording;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $currentVehicleOwner;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $numberAndNameOfTheRoad;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $complementAddress1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $postalCode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $homePhone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cellPhone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $workPhone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $circulationDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $purchaseDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $lastEventDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $brandLabel;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ModelLabel;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $version;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $vin;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $registration;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $TypeOfLead;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Mileage;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $energyLabel;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $NVSeller;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $VOSeller;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $billingComment;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $VNVOType;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $VNVOFileNumber;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $NVSalesIntermediary;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $eventDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $eventOrigin;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBusinessAccount(): ?string
    {
        return $this->businessAccount;
    }

    public function setBusinessAccount(?string $businessAccount): self
    {
        $this->businessAccount = $businessAccount;

        return $this;
    }

    public function getEventAccount(): ?string
    {
        return $this->eventAccount;
    }

    public function setEventAccount(?string $eventAccount): self
    {
        $this->eventAccount = $eventAccount;

        return $this;
    }

    public function getLastEventAccount(): ?string
    {
        return $this->lastEventAccount;
    }

    public function setLastEventAccount(?string $lastEventAccount): self
    {
        $this->lastEventAccount = $lastEventAccount;

        return $this;
    }

    public function getSheetNumber(): ?string
    {
        return $this->sheetNumber;
    }

    public function setSheetNumber(?string $sheetNumber): self
    {
        $this->sheetNumber = $sheetNumber;

        return $this;
    }

    public function getCivilityWording(): ?string
    {
        return $this->civilityWording;
    }

    public function setCivilityWording(?string $civilityWording): self
    {
        $this->civilityWording = $civilityWording;

        return $this;
    }

    public function getCurrentVehicleOwner(): ?string
    {
        return $this->currentVehicleOwner;
    }

    public function setCurrentVehicleOwner(?string $currentVehicleOwner): self
    {
        $this->currentVehicleOwner = $currentVehicleOwner;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getNumberAndNameOfTheRoad(): ?string
    {
        return $this->numberAndNameOfTheRoad;
    }

    public function setNumberAndNameOfTheRoad(?string $numberAndNameOfTheRoad): self
    {
        $this->numberAndNameOfTheRoad = $numberAndNameOfTheRoad;

        return $this;
    }

    public function getComplementAddress1(): ?string
    {
        return $this->complementAddress1;
    }

    public function setComplementAddress1(?string $complementAddress1): self
    {
        $this->complementAddress1 = $complementAddress1;

        return $this;
    }

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    public function setPostalCode(?string $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getHomePhone(): ?string
    {
        return $this->homePhone;
    }

    public function setHomePhone(?string $homePhone): self
    {
        $this->homePhone = $homePhone;

        return $this;
    }

    public function getCellPhone(): ?string
    {
        return $this->cellPhone;
    }

    public function setCellPhone(?string $cellPhone): self
    {
        $this->cellPhone = $cellPhone;

        return $this;
    }

    public function getWorkPhone(): ?string
    {
        return $this->workPhone;
    }

    public function setWorkPhone(?string $workPhone): self
    {
        $this->workPhone = $workPhone;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getCirculationDate(): ?string
    {
        return $this->circulationDate;
    }

    public function setCirculationDate(?string $circulationDate): self
    {
        $this->circulationDate = $circulationDate;

        return $this;
    }

    public function getPurchaseDate(): ?string
    {
        return $this->purchaseDate;
    }

    public function setPurchaseDate(?string $purchaseDate): self
    {
        $this->purchaseDate = $purchaseDate;

        return $this;
    }

    public function getLastEventDate(): ?string
    {
        return $this->lastEventDate;
    }

    public function setLastEventDate(?string $lastEventDate): self
    {
        $this->lastEventDate = $lastEventDate;

        return $this;
    }

    public function getBrandLabel(): ?string
    {
        return $this->brandLabel;
    }

    public function setBrandLabel(?string $brandLabel): self
    {
        $this->brandLabel = $brandLabel;

        return $this;
    }

    public function getModelLabel(): ?string
    {
        return $this->ModelLabel;
    }

    public function setModelLabel(?string $ModelLabel): self
    {
        $this->ModelLabel = $ModelLabel;

        return $this;
    }

    public function getVersion(): ?string
    {
        return $this->version;
    }

    public function setVersion(?string $version): self
    {
        $this->version = $version;

        return $this;
    }

    public function getVin(): ?string
    {
        return $this->vin;
    }

    public function setVin(?string $vin): self
    {
        $this->vin = $vin;

        return $this;
    }

    public function getRegistration(): ?string
    {
        return $this->registration;
    }

    public function setRegistration(?string $registration): self
    {
        $this->registration = $registration;

        return $this;
    }

    public function getTypeOfLead(): ?string
    {
        return $this->TypeOfLead;
    }

    public function setTypeOfLead(?string $TypeOfLead): self
    {
        $this->TypeOfLead = $TypeOfLead;

        return $this;
    }

    public function getMileage(): ?string
    {
        return $this->Mileage;
    }

    public function setMileage(?string $Mileage): self
    {
        $this->Mileage = $Mileage;

        return $this;
    }

    public function getEnergyLabel(): ?string
    {
        return $this->energyLabel;
    }

    public function setEnergyLabel(?string $energyLabel): self
    {
        $this->energyLabel = $energyLabel;

        return $this;
    }

    public function getNVSeller(): ?string
    {
        return $this->NVSeller;
    }

    public function setNVSeller(?string $NVSeller): self
    {
        $this->NVSeller = $NVSeller;

        return $this;
    }

    public function getVOSeller(): ?string
    {
        return $this->VOSeller;
    }

    public function setVOSeller(?string $VOSeller): self
    {
        $this->VOSeller = $VOSeller;

        return $this;
    }

    public function getBillingComment(): ?string
    {
        return $this->billingComment;
    }

    public function setBillingComment(?string $billingComment): self
    {
        $this->billingComment = $billingComment;

        return $this;
    }

    public function getVNVOType(): ?string
    {
        return $this->VNVOType;
    }

    public function setVNVOType(?string $VNVOType): self
    {
        $this->VNVOType = $VNVOType;

        return $this;
    }

    public function getVNVOFileNumber(): ?string
    {
        return $this->VNVOFileNumber;
    }

    public function setVNVOFileNumber(?string $VNVOFileNumber): self
    {
        $this->VNVOFileNumber = $VNVOFileNumber;

        return $this;
    }

    public function getNVSalesIntermediary(): ?string
    {
        return $this->NVSalesIntermediary;
    }

    public function setNVSalesIntermediary(?string $NVSalesIntermediary): self
    {
        $this->NVSalesIntermediary = $NVSalesIntermediary;

        return $this;
    }

    public function getEventDate(): ?string
    {
        return $this->eventDate;
    }

    public function setEventDate(?string $eventDate): self
    {
        $this->eventDate = $eventDate;

        return $this;
    }

    public function getEventOrigin(): ?string
    {
        return $this->eventOrigin;
    }

    public function setEventOrigin(?string $eventOrigin): self
    {
        $this->eventOrigin = $eventOrigin;

        return $this;
    }
}
