<?php

namespace App\Service;

use App\Entity\Client;
use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpSpreadsheet\IOFactory;

class ClientService {

    private $em;
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function import($file) {

        // Vérifie que le fichier est bien au format 'xlsx'
        if ($file->getClientOriginalExtension() === 'xlsx') {
            // Chemin temporaire vers le fichier
            $filePath = $file->getRealPath();

            // Charger le fichier Excel
            $spreadsheet = IOFactory::load($filePath);
            $worksheet = $spreadsheet->getActiveSheet();

            $firstRow = $worksheet->getRowIterator()->current();
            $headerRow = [];
            foreach ($firstRow->getCellIterator() as $cell) {
                $columnLetter = $cell->getColumn(); // Récupérer la lettre de la colonne
                $headerRow[$columnLetter] = $cell->getValue(); // Stocker le nom de la colonne associé à la lettre
            }

            // Parcourir les données du fichier
            foreach ($worksheet->getRowIterator() as $row) {

                // Traitement des données et enregistrement en base de données

                // Ignorer la première ligne (en-têtes)
                if ($row->getRowIndex() === 1) {
                    continue;
                }

                // Récupérer les valeurs de chaque colonne
                $rowData = [];
                foreach ($row->getCellIterator() as $cell) {
                    // on utilise l'index ici
                    $rowData[] = $cell->getValue();

                    // OU
                    // $columnLetter = $cell->getColumn(); // Récupérer la lettre de la colonne
                    // $columnName = $headerRow[$columnLetter]; // Obtenir le nom de la colonne à partir de l'en-tête
                    // $rowData[$columnName] = $cell->getValue();
                }

                $client = new Client();
                $client->setBusinessAccount($rowData[0]);
                $client->setEventAccount($rowData[1]);
                $client->setLastEventAccount($rowData[2]);
                $client->setSheetNumber($rowData[3]);
                $client->setCivilityWording($rowData[4]);
                $client->setCurrentVehicleOwner($rowData[5]);
                $client->setName($rowData[6]);
                $client->setFirstName($rowData[7]);
                $client->setNumberAndNameOfTheRoad($rowData[8]);
                $client->setComplementAddress1($rowData[9]);
                $client->setPostalCode($rowData[10]);
                $client->setCity($rowData[11]);
                $client->setHomePhone($rowData[12]);
                $client->setCellPhone($rowData[13]);
                $client->setWorkPhone($rowData[14]);
                $client->setEmail($rowData[15]);
                $client->setCirculationDate($rowData[16]);
                $client->setPurchaseDate($rowData[17]);
                $client->setLastEventDate($rowData[18]);
                $client->setBrandLabel($rowData[19]);
                $client->setModelLabel($rowData[20]);
                $client->setVersion($rowData[21]);
                $client->setVin($rowData[22]);
                $client->setRegistration($rowData[23]);
                $client->setTypeOfLead($rowData[24]);
                $client->setMileage($rowData[25]);
                $client->setEnergyLabel($rowData[26]);
                $client->setNVSeller($rowData[27]);
                $client->setVOSeller($rowData[28]);
                $client->setBillingComment($rowData[29]);
                $client->setVNVOType($rowData[30]);
                $client->setVNVOFileNumber($rowData[31]);
                $client->setNVSalesIntermediary($rowData[32]);
                $client->setEventDate($rowData[33]);
                $client->setEventOrigin($rowData[34]);

                $this->em->persist($client);
            }
            // Enregistrer les modifications en base de données
            $this->em->flush();

            return "Données enregistrées avec succèss";
        } else {
            throw new \Error("Erreur");
        }

    }

}